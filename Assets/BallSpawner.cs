﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject ballPrefab;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnBall());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator SpawnBall()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(5f);
            GameObject o = Instantiate(ballPrefab, transform.position, Quaternion.identity);
            o.transform.SetParent(transform);
        }
    }
}
